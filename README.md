# Packaging python package (with documentation, git and strict rules)

 - [Cheat sheet](https://jbleger.gitlab.io/python-packaging/cheatsheet.html)
 - [Slides](https://jbleger.gitlab.io/python-packaging/slides.html)
